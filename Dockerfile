FROM python:3-alpine

RUN pip3 install mkdocs mkdocs-material plantuml-markdown

EXPOSE 8000