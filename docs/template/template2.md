# テンプレート2

## Code

```javascript
function hoge() {
    return 0;
}
```

## テーブル

| 列1     | 列2     |
| ------- | ------- |
| 項目1-1 | 項目2-1 |
| 項目1-2 | 項目2-2 |

## PlantUML

[PlantUML概要](https://plantuml.com/ja/)

### アプリケーションレイヤー構成図

```plantuml
@startuml

node ノード1
node ノード2
node ノード3

ノード1-->ノード2
ノード1<--ノード2


@enduml
```

### ネットワーク

```plantuml

@startuml
nwdiag {
  network internet {
    router;
  }


  network dmz {
    address = "210.x.x.x/24"

    router [address = "172.x.x.2"];
    web01 [address = "210.x.x.1"];
    web02 [address = "210.x.x.2"];
  }

  network internal {
    address = "172.x.x.x/24";

    web01 [address = "172.x.x.1"];
    web02 [address = "172.x.x.2"];
    db01;
    db02;
  }

}
@enduml
```

### アクティビティ図(新文法)

```plantuml
@startuml

start

:処理1;

while (条件分岐?)
  :YES処理1;
  :YES処理2;
endwhile

:処理3;

stop

@enduml

```

### ガントチャート

```plantuml
@startgantt

'開始日
Project starts the 2020/3/1
saturday are closed
sunday are closed

[タスク1] lasts 3 days
[タスク2] lasts 5 days
[タスク1]->[タスク2]

@endgantt
```
